packer {
  required_version = ">= 1.7.0"
  required_plugins {
    amazon = {
      source  = "github.com/hashicorp/amazon"
      version = "~> 1"
    }
  }
}

source "amazon-ebs" "win2022" {
  profile = "tfc"
  region  = "us-east-1"

  ami_name      = "packer-windows2022-aws-${formatdate("YYYY-MM-DD_hh-mm_ZZZ", timestamp())}"
  instance_type = "t3.micro"
  vpc_id        = "vpc-07053b1eba142facf"

  subnet_id             = "subnet-01198f97d5aab1963"
  security_group_id     = "sg-02f0661f8b5364271"
  communicator          = "winrm"
  force_delete_snapshot = true

  source_ami_filter {
    filters = {
      name                = "Windows_Server-2022-English-Full-Base*"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = ["801119661308"]
  }

  winrm_username = "Administrator"
  #winrm_password = "{{ .WinRMPassword }}"
  winrm_insecure = true
  winrm_use_ssl = true
  winrm_use_ntlm = true
  winrm_timeout = "3m"
  user_data_file = "./scripts/bootstrap_win.txt"

  tags = {
    Name          = "packer-WindowsServer2022"
    Created-by    = "Packer"
    OS_Version    = "Windows Server 2022"
    Release       = "Latest"
    Base_AMI_Name = "{{ .SourceAMIName }}",
    Base_AMI_ID   = "{{ .SourceAMI }}"
    #Extra         = "{{ .SourceAMITags }}"
  }
}

build {
  name = "packer-win2022-EHS"
  sources = [
    "source.amazon-ebs.win2022"
  ]

  provisioner "powershell" {
    inline = [
      "Write-Host ('Administrator password is: {{ .WinRMPassword }}')"
    ]
  }
  #user_data_file = "./scripts/bootstrap_win.txt"
  provisioner "file" {
    source      = "./scripts/choco_install.txt"
    destination = "c:\\users\\Administrator\\"
  }

  
  

}

