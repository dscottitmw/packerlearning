packer {
  required_plugins {
    amazon = {
      version = "~> 1"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

source "amazon-ebs" "ubuntu" {
  ami_name          = "learn-packer-linux-aws"
  instance_type     = "t2.micro"
  vpc_id            = "vpc-07053b1eba142facf"
  subnet_id         = "subnet-01198f97d5aab1963"
  security_group_id = "sg-02f0661f8b5364271"

  region                 = "us-east-1"
  skip_region_validation = "true"
}

 source_ami_filter {
   filters = {
      name                = "ubuntu/images/*/ubuntu-*-22.04-amd64-server-*"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = ["099720109477"]
  }


build {
  name = "learn-packer"
  sources = [
    "source.amazon-ebs.ubuntu"
  ]
}

